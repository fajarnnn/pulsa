<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("global/g_header.php") ?>
    <title>Test First</title>
</head>

<body class="body-color">
    <div id="wrapper" >
        <header class="header-color rounded-bottom">
            <div class="container">
                <!-- Content here -->
                <div class="">
                    <div class="d-flex justify-content-center"
                        style="height:200px; width:100%; max-height:200px;display:inline-grid;align-items:center">
                        <img style="height:150px; width:150px;max-width-150px; max-height:150px;"
                            class="rounded-bottom" src="<?= base_url("assets/img/SUPERMAN.png") ?>"></img>
                    </div>
                </div>

                <!-- col- elements here -->
            </div>
        </header>
        <div class="container">
        <div class="row carousel-pad">
                <div class="col-12 nopaddingonly">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="bi bi-search"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Cari" aria-label="Cari"
                            aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="row carousel-pad">
                <div class="col-lg-12" style="padding: 0 !important">
                    <div id="carouselExampleIndicators" class="carousel slide padding-side" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100"
                                    src="https://www.extremetech.com/wp-content/uploads/2019/12/SONATA-hero-option1-764A5360-edit.jpg"
                                    alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100"
                                    src="https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5d35eacaf1176b0008974b54%2F0x0.jpg%3FcropX1%3D790%26cropX2%3D5350%26cropY1%3D784%26cropY2%3D3349"
                                    alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100"
                                    src="https://img.etimg.com/thumb/msid-73268134,width-640,resizemode-4,imgsize-35417/surprise-heard-of-a-sony-car.jpg"
                                    alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <?php foreach ($data as $category => $value) {
                $icon = "";
                if ($category == "Games")
                    $icon = "controller";
                else if ($category == "Pulsa")
                    $icon = "phone";
                else
                    $icon = "globe";
                ?>
                <div class="row carousel-pad">
                    <div class="col-12 col-lg-3 col-md-3 col-xs-12 category padding-side"
                        style="padding-top: 0 !important;">
                        <a id="<?= $category ?>" class="cursor-pointer active">
                            <i class="bi bi-<?= $icon ?>"></i>
                            <?= $category ?>
                        </a>
                    </div>
                </div>
                <div class="row carousel-pad">
                    <?php foreach ($value as $arr => $field) { ?>
                    
                    <!--<div class="box3-img" style="background-image: url('<?= base_url("assets/img/" . $field['brand'] . ".png")?>')">-->
                        
                    <!--</div>-->

                        <div class="col-md-3 col-4 text-center padding-side"
                            style="display: inline-block !important;float: left; padding-bottom: 5%">
                            <a href="" >
                                <img class="w-100 h-100" src="<?= base_url("assets/img/" . $field['brand'] . ".png")?>">
                                <img class="w-100 above-img" src="<?= base_url("assets/img/wavetwo.png") ?>">
                               
                            </a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <br>
    <br>
    <?php include("global/g_footer.php") ?>
</body>

</html>