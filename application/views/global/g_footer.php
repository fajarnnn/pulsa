<nav class="navbar fixed-bottom navbar-dark bg-dark center-nav">
  <a class="navbar-brand" href="#" style="margin: 0 !important; width:100%">
    <div class="row">
        <div class="col-4 footer-font"><i class="bi bi-house"></i></div>
        <div class="col-4 footer-font"><i class="bi bi-clock-history"></i></div>
        <div class="col-4 footer-font"><i class="bi bi-person-circle"></i></div>
    </div>
    <div class="row">
        <div class="col-4 footer-font">Home</div>
        <div class="col-4 footer-font ">Riwayat</div>
        <div class="col-4 footer-font">Akun</div>
    </div>
  </a>
</nav>
<script src="<?= base_url('assets/js/jquery-3.6.3.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
