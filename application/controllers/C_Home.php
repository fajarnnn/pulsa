<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ApiServices');
        $this->load->model('product');
    }
    public function index()
    {
        $priceList = $this->product->getProduct();
        // $data = aray;
        foreach ($priceList as $row) {

            $data[$row->category][] =
                array(
                    'name' => $row->name,
                    'brand' => $row->brand,
                    'type' => $row->type,
                    'seller_name' => $row->seller_name,
                    'price' => $row->price,
                    'buyer_sku_code' => $row->buyer_sku_code,
                    'buyer_status' => $row->buyer_status,
                    'seller_status' => $row->seller_status,
                    'unlimited_stock' => $row->unlimited_stock,
                    'stock' => $row->stock,
                    'multi' => $row->multi,
                    'start_cut_off' => $row->start_cut_off,
                    'end_cut_off' => $row->end_cut_off,
                    'desc' => $row->description
                );
        }
        // print_r($data);
        $v_data['data'] = $data;
        // echo "finish";
        $this->load->view('v_home', $v_data);
    }
    public function feedData()
    {
        $aps = $this->apiservices;
        // $this->apiservices = new ApiServices();
        $param = array("isJson" => false);
        $priceList = $aps->callAPI("pricelist", $param)->data;
        $id = $this->product->getLastID();
        foreach ($priceList as $plf => $val) {
            $insertData = array(
                'name' => $val->product_name,
                'category' => $val->category,
                'brand' => $val->brand,
                'type' => $val->type,
                'seller_name' => $val->seller_name,
                'price' => $val->price,
                'buyer_sku_code' => $val->buyer_sku_code,
                'buyer_status' => $val->buyer_product_status,
                'seller_status' => $val->seller_product_status,
                'unlimited_stock' => $val->unlimited_stock,
                'stock' => $val->stock,
                'multi' => $val->multi,
                'start_cut_off' => $val->start_cut_off,
                'end_cut_off' => $val->end_cut_off,
                'description' => $val->desc
            );
            $this->db->replace('products', $insertData);
        }
        echo "finish";
    }
}

/* End of file  C_Home.php */