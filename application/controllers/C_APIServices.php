<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class C_APIServices extends CI_Controller {

    public function index($cmdName, $arrayData)
    {
        switch ($cmdName){
            case 'pricelist':
                $result = $this->getPriceList();
                break;
            case 'transaction':
                $result = $this->doTransaction($arrayData);
        }
        return $result;
        
    }
    public function callAPI($cmdName, $arrayData)
    {
        switch ($cmdName){
            case 'pricelist':
                $result = $this->getPriceList();
                break;
            case 'transaction':
                $result = $this->doTransaction($arrayData);
        }
        return $result;
        
    }
    private function getPriceList(){
        $sign = md5(constant("API_USERNAME").constant("API_KEY")."pricelist");
        
        $data = array(
            'cmd' => 'prepaid',
            'username' => constant("API_USERNAME"),
            'sign'    => $sign
        );
        $data_string = json_encode($data);

        $curl = curl_init('https://api.digiflazz.com/v1/price-list');

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data

        // Send the request
        $result = curl_exec($curl);
        
        return $result;
    }
    private function doTransaction($param){
        $sign = md5(constant("API_USERNAME").constant("API_KEY").$param->refId);
        $data = array(
                'username' => 'picevooErxmD',
                'buyer_sku_code' => $param->sku,
                'customer_no' => $param->customero,
                'ref_id' => $param->refId,
                'testing' => true,
                'sign'    => $sign
        );
        $data_string = json_encode($data);

        // $curl = curl_init('https://api.digiflazz.com/v1/price-list');
        $curl = curl_init('https://api.digiflazz.com/v1/transaction');

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data

        // Send the request
        $result = curl_exec($curl);

        // Free up the resources $curl is using
        curl_close($curl);

        return $result;
    }
}
        
    /* End of file  C_APIServices.php.php */
        
                            