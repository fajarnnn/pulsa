<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class FeedingData extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->library('ApiServices');
        $this->load->model('product');
    }
    
    public function index()
    {
                    
    }
    public function feedPriceList(){
        $aps = $this->apiservices;
        // $this->apiservices = new ApiServices();
        $param = array("isJson" => false);
        $priceList = $aps->callAPI("pricelist",$param)->data;
        $id = $this->product->getLastID();
        foreach ($priceList as $plf => $val) {
            $insertData = array(
                'name' => $val->product_name,
                'category' => $val->category,
                'brand' => $val->brand,
                'type' => $val->type,
                'seller_name' => $val->seller_name,
                'price' => $val->price,
                'buyer_sku_code' => $val->buyer_sku_code,
                'buyer_status' => $val->buyer_product_status,
                'seller_status' => $val->seller_product_status,
                'unlimited_stock' => $val->unlimited_stock,
                'stock' => $val->stock,
                'multi' => $val->multi,
                'start_cut_off' => $val->start_cut_off,
                'end_cut_off' => $val->end_cut_off,
                'description' => $val->desc
            );
            $this->db->replace('products', $insertData);
        }
        echo "finish";
    }
        
}
        
    /* End of file  FeedingData.php */
        
                            